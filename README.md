ARintB: Augmented Reality in the Browser 
========================================

This repository is about adding AR (Augmented Reality) / VR (Virtual Reality) / MR (Mixed Reality) experiences to websites and webapps, using only platform-independent web technologies (HTML5, JavaScript & CSS).

The idea is to use the sensors on a mobile device to get:

- the location whithin 5-10 m (from GPS)
- the screen orientation (portrait, landscape, rotated portrait, rotated landscape)
- the heading from the magnetic compass measurement
- the gravity vector from the accelerometer
- the relative 3D orientation in space of the device from the gyroscopes

then processing these data locally on the device using JavaScript. No image processing, no SLAM, no markers.

Tested on a handful of mid-range devices: 2015 Apple iPad Air with iOS 12, Sony Xperia XA2 with Android 8.

# experiments and proof-of-concepts

1. [horizon](horizon.html): displays the horizon line and a vertical plummet; single file without JS dependencies

# settings

# iOS

Location services must be active for Safari:

![location](img/location.png "Enable location services")

Additionally, on iOS 12.2+ the user has to manually allow access to the motion sensors in Settings > Safari > Motion & Orientation Access.

![motion](img/motion.png "Enable motion sensors")

# Android

TBD
